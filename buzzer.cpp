//
// Libary for piezo buzzer control.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#include "arduino.h"
#include "buzzer.h"
#include "notes.h"


//--------------------------------------------------------------------------

Buzzer::Buzzer() :
   Tickable(),
   m_pinBuzzer(0),
   m_buzzerState(Idle),
   m_melodyNotes(NULL),
   m_melodyDurations(NULL),
   m_melodyLength(0),
   m_melodyNoteIndex(0),
   m_tonePlayEndTime(0),
   m_repeatOn(false),
   m_playRequestCount(0)
{
   
}

//--------------------------------------------------------------------------

void Buzzer::begin(int pin)
{
   m_pinBuzzer = pin;
   
   pinMode(m_pinBuzzer, OUTPUT);
}

//--------------------------------------------------------------------------

bool Buzzer::setMelody(const char* notes, const char* durations)
{
   int notesLength     = strlen(notes);
   int durationsLength = strlen(durations);
   
   if (notesLength == durationsLength)
   {
      m_melodyNotes     = notes;
      m_melodyDurations = durations;
      m_melodyLength    = notesLength;
   }   
   
   return (notesLength == durationsLength);
}

//--------------------------------------------------------------------------

void Buzzer::play(bool repeatOn)
{
   m_melodyNoteIndex = 0;
   m_repeatOn        = repeatOn;
   m_buzzerState     = PlayNextNote;
}

//--------------------------------------------------------------------------

void Buzzer::stop()
{
   m_melodyNoteIndex = 0;
   m_buzzerState     = Idle;
}

//--------------------------------------------------------------------------

void Buzzer::requestPlay()
{
   if (m_playRequestCount == 0)
   {
      play(m_repeatOn);
   }   
   
   m_playRequestCount++;
}

//--------------------------------------------------------------------------

void Buzzer::requestStop()
{
   m_playRequestCount--;
   
   if (m_playRequestCount == 0)
   {
      stop();
   }
}

//--------------------------------------------------------------------------

void Buzzer::onTick()
{
   const unsigned long currentTime = localTime();

   switch (m_buzzerState)
   {
      case Idle:
      {
         noTone(m_pinBuzzer);    // make sure it is quiet
         resetLocalTime();       // to avoid clock overflow
      }
      break;
      
      case PlayNextNote:
      {
         if (m_melodyNoteIndex < m_melodyLength)
         {
            unsigned int frequencyHz = Note::toFrequency(m_melodyNotes[m_melodyNoteIndex]);
            unsigned long durationMs = Note::toDuration(m_melodyDurations[m_melodyNoteIndex]);
            
            m_tonePlayEndTime = currentTime + durationMs + pauseBetweenNotesMs;
            
            if (frequencyHz > 0)
            {
               tone(m_pinBuzzer, frequencyHz, durationMs);              
            }           
                        
            m_melodyNoteIndex++;
            
            m_buzzerState = WaitForNextNote;
         }
         else if (m_repeatOn)
         {
            m_melodyNoteIndex = 0;
            m_buzzerState     = PlayNextNote;
         }
         else
         {
            m_melodyNoteIndex = 0;
            m_buzzerState     = Idle;
         }
      }
      break;
      
      case WaitForNextNote:
      {
         if (currentTime > m_tonePlayEndTime)
         {
            noTone(m_pinBuzzer); // make sure the tone has ended (it should have by now)
            m_buzzerState = PlayNextNote;                        
         }
      }
      break;
   }            
}


