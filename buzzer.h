//
// Libary for piezo buzzer control.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#ifndef _BUZZER_H_
#define _BUZZER_H_

#include "arduino.h"
#include "tickable.h"

class Buzzer : public Tickable
{
   public:
   
      // Constructor. Make sure to call begin() before the other methods are used.
      Buzzer();
      
      // Initializes the Buzzer.
      void begin(int pin);      
      
      // Sets the melody that the Buzzer shall play. The melody is defined by two
      // null terminated strings, one containing the notes and one containing 
      // the duration of each note.
      //  Available notes    : 'a'-'g' (440-784 Hz) and 'A'-'G' (880-1568 Hz)
      //  Available durations: '1'-'8' where '1' = 1/8, '2' = 2/8 etc.
      bool setMelody(const char* notes, const char* durations);
      
      // Turns repeat on, i.e. the melody is played again and again when started. 
      void repeatOn() { m_repeatOn = true; };
      
      // Turns repeat off, i.e. the melody is only played once when started.
      void repeatOff() { m_repeatOn = false; };
      
      // Starts to play the melody (if not already playing).
      void play(bool repeatOn = false);
      
      // Stops playing the melody (if playing).
      void stop();
      
      // If the number requests to play is equal to the number requests to stop + 1,
      // the melody starts to play.
      void requestPlay();
      
      // If the number requests to stop is equal to the number requests to play,
      // the melody is stopped.
      void requestStop();
      
      // Call tick() periodically to make the Buzzer function, at least with a period
      // shorter than the longest note in the melody (the duration of a 1/8 note is 150 ms).
      virtual void onTick();      
            
   private:
   
      typedef enum {Idle, PlayNextNote, WaitForNextNote} BuzzerState;
      
      const unsigned long pauseBetweenNotesMs = 10ul;
      
      int            m_pinBuzzer;
      BuzzerState    m_buzzerState;
      const char*    m_melodyNotes;
      const char*    m_melodyDurations;
      int            m_melodyLength;
      int            m_melodyNoteIndex;
      unsigned long  m_tonePlayEndTime;
      bool           m_repeatOn;
      int            m_playRequestCount;
   
};

#endif // _BUZZER_H_