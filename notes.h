//
// Support library for notes.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#ifndef _NOTE_H_
#define _NOTE_H_

#include "arduino.h"

class Note
{
   public:
   
      // Constructor
      Note(char note, char duration);
   
      // Static method to covert a note represented by a character
      // to a frequency. The follwing characters can be used for notes:
      // 'a'-'g' (440-784 Hz) and 'A'-'G' (880-1568 Hz).
      static unsigned int toFrequency(char note);
      
      // Static method to convert a note duration represented by a character
      // to a time duration in milliseconds. The following durations can be used:
      //  '1' : 1/8 (150 ms)
      //  '2' : 2/8
      //     ...
      //  '8' : 8/8
      static unsigned long toDuration(char duration); 
      
      // Returns the frequency of the note in Hz.
      unsigned int frequency() { return m_frequency; };
      
      // Returns the duration of the note in milliseconds.
      unsigned long duration() { return m_duration; };
      
   private:
   
      Note() { };
   
      unsigned int  m_frequency; 
      unsigned long m_duration;
      
      static const unsigned long baseTimeMs = 150ul;  // duration of a 1/8 note      
      
};


#endif // _NOTE_H_