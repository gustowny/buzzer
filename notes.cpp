//
// 
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#include "notes.h"


#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976


const unsigned int frequenciesLow[] 
{
    440,  // a
    494,  // b
    523,  // c
    587,  // d
    659,  // e
    698,  // f
    784,  // g
};

const unsigned int frequenciesHigh[] 
{
    880,  // A
    988,  // B
   1047,  // C 
   1175,  // D
   1319,  // E
   1397,  // F
   1568,  // G
};

//--------------------------------------------------------------------------

Note::Note(char note, char duration)
{
   m_frequency = Note::toFrequency(note);
   m_duration  = Note::toDuration(duration);
}

//--------------------------------------------------------------------------

unsigned int Note::toFrequency(char note)
{
   if (note >= 'a' && note <= 'g')
   {
      return frequenciesLow[(byte)(note - 'a')];
   }
   else if (note >= 'A' && note <= 'G')
   {
      return frequenciesHigh[(byte)(note - 'A')];
   }
   else
   {
      return 0;
   }
}

//--------------------------------------------------------------------------

unsigned long Note::toDuration(char duration)
{
   unsigned long milliseconds = 0;
   
   if (duration >= '1' && duration <= '8')
   {
      milliseconds = (unsigned long)(duration - '0') * baseTimeMs;
   }
   
   return milliseconds;
}

//--------------------------------------------------------------------------
